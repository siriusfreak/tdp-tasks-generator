import numpy as np

def solve_brown_robinson(a, eps, max_iter=-1):
    n, m = a.shape
    a_counts = np.zeros(n)
    b_counts = np.zeros(m)

    B = np.zeros(m) + a[np.argmax(np.max(a, axis=1)), :]
    A = np.zeros(n)

    V_avg = 0

    k = 1
    while True:
        B_min_ind = np.argmin(B)
        b_counts[B_min_ind] += 1
        A += a[:, B_min_ind]

        V_low = np.min(B) / k
        V_high = np.max(A) / k

        V_last = V_avg
        V_avg = (V_low + V_high) / 2

        if abs(V_avg - V_last) < eps and k > 10:
            print(f"{V_low} {V_high} {k}")
            break

        if max_iter != -1 and k == max_iter:
            break

        k += 1

        A_max_ind = np.argmax(A)
        a_counts[A_max_ind] += 1
        B += a[A_max_ind, :]

    p = a_counts / k
    q = b_counts / k

    return p, q, V_avg