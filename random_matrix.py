import math
import numpy as np

def gen_random_matrix(n, m, a_min, a_max, to_remove_rows, to_remove_cols):
    res = (np.random.rand(n, m) * (a_max - a_min) + a_min).round()

    for _ in range(to_remove_cols):
        a = 0
        b = 0

        used = set()
        while a == b or (a in used) or (b in used):
            a = np.random.randint(0, m)
            b = np.random.randint(0, m)
        used.add(a)
        used.add(b)
        for i in range(0, n):
            if res[i, a] > res[i, b]:
                res[i, [a, b]] = res[i, [b, a]]
    
    for _ in range(to_remove_rows):
        a = 0
        b = 0
    
        used = set()
        while a == b or (a in used) or (b in used):
            a = np.random.randint(0, n)
            b = np.random.randint(0, n)
        used.add(a)
        used.add(b)
        for i in range(0, m):
            if res[a, i] > res[b, i]:
                res[[a, b], i] = res[[b, a], i]
    
    np.random.default_rng().shuffle(res, axis=0)
    np.random.default_rng().shuffle(res, axis=1)

    return res

def simplify_matrix(a):
    i = 0
    while i < len(a) - 1:
        row_a = a[i, :]
        row_b = a[i + 1, :]
        row_comp = row_a >= row_b
        if row_comp.all():
            a = np.delete(a, i + 1, 0)
        elif np.logical_not(row_comp).all():
            a = np.delete(a, i, 0)
        else:
            i += 1
    
    i = 0
    while i < len(a[0]) - 1:
        col_a = a[:, i]
        col_b = a[:, i + 1]
        col_comp = col_a >= col_b
        if col_comp.all():
            a = np.delete(a, i + 1, 1)
        elif np.logical_not(col_comp).all():
            a = np.delete(a, i, 1)
        else:
            i += 1
    
    return a



def generate_matrix(n, m, a_min, a_max, to_remove_rows, to_remove_cols):
    while True:
        a = gen_random_matrix(n, m, a_min, a_max, to_remove_rows, to_remove_cols)
        a_sim = simplify_matrix(a)
        a_shape = a.shape
        a_sim_shape = a_sim.shape
        if abs(a_shape[0] - a_sim_shape[0]) + abs(a_shape[0] - a_sim_shape[0]) >= 8:
            print(a)
            print(a_sim)
            return a



def matrix_to_tex(a):
    res = ""
    for i in range(len(a)):
        res += " &".join(map(str, map(int, list(a[i])))) + "\\\\\n"
    return res

