import numpy as np

from itertools import chain, combinations

def powerset(iterable):
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))


def kkt_solve(a, p_select, q_select, mode):
    if len(p_select) == 0 or len(q_select) == 0:
        return None

    if len(p_select) == 1 and mode == 'p':
        res = [0] * a.shape[0]
        res[p_select[0]] = 1
        return res

    if len(q_select) == 1 and mode == 'q':
        res = [0] * a.shape[1]
        res[q_select[0]] = 1
        return res

    grid = np.ix_(p_select, q_select)
    a_cur = a[grid]

    if mode == 'p':
        a_cur = np.transpose(a_cur)

    n = a_cur.shape[0]
    m = a_cur.shape[1]


    sign = 1
    if mode == 'p':
        sign = -1

    a_cur = np.hstack((a_cur, sign*np.ones((n, 1))))

    a_cur = np.vstack((a_cur, np.array([1] * m + [0])))

    b = [0] * n + [1]

    try:
        x = np.linalg.solve(a_cur, b)

        if mode == 'p':
            res = np.zeros(a.shape[0])
            res[p_select] = x[:-1]
        else:
            res = np.zeros(a.shape[1])
            res[q_select] = x[:-1]
        return res
    except np.linalg.LinAlgError:
        return None


def solve_kkt(a):
    tmp = a[0:1, :]
    print(tmp)
    print(tmp.shape)

    p_prem = list(powerset([0, 1, 2]))
    q_prem = list(powerset([0, 1, 2, 3]))

    p_all = []
    q_all = []

    for p_select in p_prem:
        for q_select in q_prem:
            p_select = np.array(p_select)
            q_select = np.array(q_select)
            p = kkt_solve(a, p_select, q_select, 'p')
            q = kkt_solve(a, p_select, q_select, 'q')
            if p is not None and q is not None and np.min(p) >= 0 and np.min(q) >= 0:
                print(f'p: {p} q: {q}')
                p_all.append(p)
                q_all.append(q)

    result = []
    for p in p_all:
        for q in q_all:
            V = np.sum(np.dot(a, q) * p)

            balance = True
            for p_prime in p_all:
                V_cur = np.sum(np.dot(a, q) * p_prime)
                if not (V_cur <= V or abs(V - V_cur) < 1e-5):
                    balance = False
                    break

            if not balance:
                continue

            for q_prime in q_all:
                V_cur = np.sum(np.dot(a, q_prime) * p)
                if not (V <= V_cur or abs(V - V_cur) < 1e-5):
                    balance = False
                    break

            if balance:
                result.append({
                    'p': p,
                    'q': q,
                    'V': V
                })

    return result