from os import path, makedirs, system
from shutil import copyfile
import errno
from random_matrix import generate_matrix, matrix_to_tex
from random_tree import random_tree_to_file
import random

tex_template = "template.tex"

def gv_to_png(gv_file, png_file):
    cmd = f'dot -Tpng "{gv_file}" -o "{png_file}"'
    system(cmd)

def gv_to_svg(gv_file, svg_file):
    cmd = f'dot -Tsvg "{gv_file}" -o "{svg_file}"'
    system(cmd)

def prepare_template(tex_template, tex_out, params):
    with open(tex_template, 'r') as file:
        data = file.read()
        for key, value in params.items():
            data = data.replace(key, value)
        with open(tex_out, "w") as f:
            f.write(data)

def tex_to_pdf(tex_file, pdf_file):
    cmd = f"latexmk -pdf -pv -quiet \"{tex_file}\""
    system(cmd)
    copyfile("task.pdf", pdf_file)

def generate_variant(student_name):
    res_dir = path.join("results_3", student_name)

    try:
        makedirs(res_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise 

    tex_file = path.join("results_3", student_name, "task.tex")
    pdf_file = path.join("results_3", student_name, "task.pdf")

    m = matrix_to_tex(generate_matrix(7, 9, 1, 10, 0, 0))

    p = [random.randint(1,10), random.randint(10,20)]
    t = [random.randint(1,5), random.randint(5,10), random.randint(10,15), random.randint(15,20)]

    probs = [random.random() for i in range(0, 9)]

    prepare_template(tex_template, tex_file, {
        "ФИО": student_name,
        "МНОЖИТЕЛЬ": str(random.randint(0,5) * 2),
        "2_a": str(random.randint(0,10)),
        "2_b": str(random.randint(0,10)),
        "2_c": str(random.randint(0,10)),
        "p_1": str(p[0]),
        "p_2": str(p[1]),
        "t_0": str(t[0]),
        "t_1": str(t[1]),
        "t_2": str(t[2]),
        "t_3": str(t[3]),
        "МАТРИЦА": m,
        "probs": ",".join(probs),
        "shg_l": str(random.randint(0,3)),
        "shg_r": str(random.randint(0,3)),
    })

    tex_to_pdf(tex_file, pdf_file)


students = [
"Демина Наталия Анатольевна",
"Зозуля Валерий Валерьевич",
"Люкова Алина Геннадьевна",
"Машарова Ольга Владимировна",
"Родионов Иван Владимирович",
"Ромашов Михаил Константинович",
"Сачкова Ольга Владимировна",
"Тимченко Анастасия Сергеевна",
"Лобзова Екатерина Владимировна",
"Белка Сергей Николаевич",
"Болгов Антон Алексеевич",
"Будылин Егор Владимирович",
"Волобуев Виталий Олегович",
"Волобуев Николай Олегович",
"Гоплачева Диана Олеговна",
"Городилов Илья Павлович",
"Егорова Светлана Михайловна",
"Зорнин Роман Алексеевич",
"Клокова Ольга Юрьевна",
"Ковбаса Вадим Дмитриевич",
"Локтионов Глеб Сергеевич",
"Мазаев Денис Алексеевич",
"Мальков Алексей Владимирович",
"Николайчук Сергей Юрьевич",
"Перминов Роман Валерьевич",
"Сидорова Юлия Александровна",
"Склярова Ирина Ивановна",
"Соколов Григорий Геннадьевич",
"Федунова Татьяна Дмитриевна",
"Хворов Александр Сергеевич",
"Акиньшин Андрей Викторович",
"Горохов Пётр Павлович",
"Греков Андрей Вячеславович",
"Елисеев Антон Александрович",
"Изиляев Игорь Романович",
"Ковалева Елена Андреевна",
"Колбасин Никита Константинович",
"Колосков Роман Юрьевич",
"Кондрашов Матвей Андреевич",
"Коновалов Никита Александрович",
"Кочнов Алексей Андреевич",
"Кучеренко Анастасия Евгеньевна",
"Морозов Дмитрий Александрович",
"Мочалов Алексей Владимирович",
"Мякотин Андрей Владимирович",
"Орлов Илья Сергеевич",
"Паршин Дмитрий Николаевич",
"Ритерман Виктория Владимировна",
"Савинова Дарья Андреевна",
"Семенкин Дмитрий Александрович",
"Сопильняк Кристина Владимировна",
"Томко Мария Владиславовна",
"Филимонов Климент Александрович",
"Шмонин Алексей Николаевич",
"Юсипов Рушан Шамилевич",
"Андреев Данила Юрьевич",
"Афанасьев Андрей Евгеньевич",
"Вострокнутов Андрей Юрьевич",
"Дубина Дарья Сергеевна",
"Евдаков Вячеслав Сергеевич",
"Ефимов Илья Викторович",
"Ильин Сергей Владиславович",
"Комарова Анна Васильевна",
"Коновалов Михаил Витальевич",
"Лабуз Илья Вячеславович",
"Лобзова Екатерина Владимировна",
"Мизеровская Анна Андреевна",
"Мухортов Илья Александрович",
"Романов Антон Сергеевич",
"Рудаков Андрей Павлович",
"Самойлов Андрей Александрович",
"Сафонов Егор Игоревич",
"Сачков Антон Леонидович",
"Семешко Дария Андреевна",
"Смирнов Андрей Юрьевич",
"Стожаров Кирилл Владимирович",
"Трифонов Максим Романович",
"Филимонов Сергей Юрьевич",
"Шепелов Александр Александрович",
"Шепелова Наталья Анатольевна",
]


for el in students[:1]:
    print(f'Generate variant for :{el}')
    generate_variant(el)