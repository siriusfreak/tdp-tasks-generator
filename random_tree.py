import typing as t
import random as rnd
import string

class Node:
    def __init__(self, value, childrens: t.List["Node"]):
        self.childrens: t.List[Node]= childrens
        self.value = value
        self.id = ''.join(rnd.choice(string.ascii_uppercase) for _ in range(8))
        self.a = 0

    def add_children(self, children: "Node"):
        self.childrens.append(children)



max_deep = 4
max_children_first_level = 2
max_children = 4
min_children = 2
min_value = 0
max_value = 15

def generate_random_tree(cur_deep = 0):
    if cur_deep == max_deep:
        value = rnd.randint(min_value, max_value)
        return Node(value, [])

    if cur_deep == 0:
        child_cound = rnd.randint(min_children, max_children_first_level)
    else:
        child_cound = rnd.randint(min_children, max_children)
    childrens = []
    for _ in range(child_cound):
        childrens.append(generate_random_tree(cur_deep+1))

    return Node("", childrens)

def out_node_list_s(tree: Node) -> str:
    res = f"{tree.id} [shape=circle, label=\"{tree.value}\"]\n"
    for child in tree.childrens:
        res = res + out_node_list_s(child)
    return res

def out_node_connections(tree: Node) -> str:
    res = ""
    for child in tree.childrens:
        res = res + f"{tree.id} -> {child.id}\n" + out_node_connections(child)

    return res

def out_tree_s(tree: Node) -> str:
    res = '\n'.join(["digraph D {", out_node_list_s(tree), 
        out_node_connections(tree), "}"])
    return res

def random_tree_to_file(filename):
    tree = generate_random_tree(0)

    with open(filename, "w") as f:
        f.write(out_tree_s(tree))